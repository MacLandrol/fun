function conwaycode(seed, number, speed)
%Seed d�signe la matrice originale, soyez imaginatif
%Number d�signe le nombre de cycle du jeu.
%Une cellule morte poss�dant exactement trois voisines vivantes devient vivante
%Une cellule vivante poss�dant deux ou trois voisines reste vivante sinon elle meurt.
persistent lut;

for i= 1:number
    hold on;
    lut = makelut(@conwayrule, 3);
    seed=bwlookup(seed, lut);
    imshow(seed);
    drawnow;
    pause(speed);
    
end
end

%-------------------------------------------------------------------------%

function out = conwayrule(nhood)
%Les lois de conways
voisin=sum(nhood(:))- nhood(2,2);
if(nhood(2,2)==1) %cellule vivante
    if voisin <=1
        out =0;
    elseif voisin>=4
        out=0;
    else
        out=1;
    end
else %cellule deja morte
    if voisin==3
        out=1;
    else
        out=0;
    end
end
end

